package com.eng.imdbclone

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomnavigation.BottomNavigationView
import org.json.JSONObject
import java.lang.Exception
import java.net.HttpURLConnection
import java.net.URL

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        println("POKRECE SE MAIN")

        DataGlobal.allTheBestMoviesComingThisSummerMovieList.clear()
        DataGlobal.upcomingHorrorMoviesWeCantWaitToSeeMovieList.clear()
        DataGlobal.whatTVShowsAreRenewedOrCanceledMovieList.clear()
        DataGlobal.allTheLatestMovieAndTVPostersMovieList.clear()
        DataGlobal.fanFavoritesMovieList.clear()

        setNavigation()
        DataGlobal.addMoviesToLists()

        createMoviesFromIDs()
    }

    fun setNavigation() {
        findViewById<BottomNavigationView>(R.id.bottomNavigationView).setupWithNavController(findNavController(R.id.fragment))
    }

    fun createMoviesFromIDs() {
        for (i in DataGlobal.allTheBestMoviesComingThisSummer) {
            val url = "https://www.omdbapi.com/?i=${i}&apikey=64072e92"
            println("allthebestmovies: ${i}")
            loadIMDbDataJSON(DataGlobal.allTheBestMoviesComingThisSummerMovieList, 1).execute(url)
        }

        for (i in DataGlobal.upcomingHorrorMoviesWeCantWaitToSee) {
            val url = "https://www.omdbapi.com/?i=${i}&apikey=64072e92"
            println("upcoming: ${i}")
            loadIMDbDataJSON(DataGlobal.upcomingHorrorMoviesWeCantWaitToSeeMovieList, 2).execute(url)
        }

        for (i in DataGlobal.whatTVShowsAreRenewedOrCanceled) {
            val url = "https://www.omdbapi.com/?i=${i}&apikey=64072e92"
            println("whattvshows: ${i}")
            loadIMDbDataJSON(DataGlobal.whatTVShowsAreRenewedOrCanceledMovieList, 3).execute(url)
        }

        for (i in DataGlobal.allTheLatestMovieAndTVPosters) {
            val url = "https://www.omdbapi.com/?i=${i}&apikey=64072e92"
            println("allthelatest: ${i}")
            loadIMDbDataJSON(DataGlobal.allTheLatestMovieAndTVPostersMovieList, 4).execute(url)
        }

        for (i in DataGlobal.fanFavorites) {
            val url = "https://www.omdbapi.com/?i=${i}&apikey=64072e92"
            loadIMDbDataJSON(DataGlobal.fanFavoritesMovieList, 5).execute(url)
        }
        setFeaturedTodayImages()
        setFanFavoritesData()
    }

    fun setFeaturedTodayImages() {
        if (DataGlobal.allTheBestMoviesComingThisSummerMovieList.count() == 6)
            loadImageFromURL(findViewById(R.id.image1_1)).execute(DataGlobal.allTheBestMoviesComingThisSummerMovieList[5].poster)
        if (DataGlobal.allTheBestMoviesComingThisSummerMovieList.count() == 18)
            loadImageFromURL(findViewById(R.id.image1_2)).execute(DataGlobal.allTheBestMoviesComingThisSummerMovieList[17].poster)
        if (DataGlobal.allTheBestMoviesComingThisSummerMovieList.count() == 25)
            loadImageFromURL(findViewById(R.id.image1_3)).execute(DataGlobal.allTheBestMoviesComingThisSummerMovieList[24].poster)

        if (DataGlobal.upcomingHorrorMoviesWeCantWaitToSeeMovieList.count() == 2)
            loadImageFromURL(findViewById(R.id.image3_1)).execute(DataGlobal.upcomingHorrorMoviesWeCantWaitToSeeMovieList[1].poster)
        if (DataGlobal.upcomingHorrorMoviesWeCantWaitToSeeMovieList.count() == 9)
            loadImageFromURL(findViewById(R.id.image3_2)).execute(DataGlobal.upcomingHorrorMoviesWeCantWaitToSeeMovieList[8].poster)
        if (DataGlobal.upcomingHorrorMoviesWeCantWaitToSeeMovieList.count() == 17)
            loadImageFromURL(findViewById(R.id.image3_3)).execute(DataGlobal.upcomingHorrorMoviesWeCantWaitToSeeMovieList[16].poster)

        if (DataGlobal.whatTVShowsAreRenewedOrCanceledMovieList.count() == 4)
            loadImageFromURL(findViewById(R.id.image5_1)).execute(DataGlobal.whatTVShowsAreRenewedOrCanceledMovieList[3].poster)
        if (DataGlobal.whatTVShowsAreRenewedOrCanceledMovieList.count() == 11)
            loadImageFromURL(findViewById(R.id.image5_2)).execute(DataGlobal.whatTVShowsAreRenewedOrCanceledMovieList[10].poster)
        if (DataGlobal.whatTVShowsAreRenewedOrCanceledMovieList.count() == 13)
            loadImageFromURL(findViewById(R.id.image5_3)).execute(DataGlobal.whatTVShowsAreRenewedOrCanceledMovieList[12].poster)

        if (DataGlobal.allTheLatestMovieAndTVPostersMovieList.count() == 3)
            loadImageFromURL(findViewById(R.id.image8_1)).execute(DataGlobal.allTheLatestMovieAndTVPostersMovieList[2].poster)
        if (DataGlobal.allTheLatestMovieAndTVPostersMovieList.count() == 5)
            loadImageFromURL(findViewById(R.id.image8_2)).execute(DataGlobal.allTheLatestMovieAndTVPostersMovieList[4].poster)
        if (DataGlobal.allTheLatestMovieAndTVPostersMovieList.count() == 6)
            loadImageFromURL(findViewById(R.id.image8_3)).execute(DataGlobal.allTheLatestMovieAndTVPostersMovieList[5].poster)
    }

    fun setFanFavoritesData() {
        println("POKRECE SE ADAPTER U MAIN")
        val adapter = MovieCell1Adapter(DataGlobal.fanFavoritesMovieList)
        val recyclerView = findViewById<RecyclerView>(R.id.fanFavoritesRecyclerView)
        recyclerView.adapter = adapter
        recyclerView.setLayoutManager(LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false))
    }

    fun createMovie(jsonObject: JSONObject, movieRatingList: ArrayList<MovieRating>, hasNull: Boolean) : Movie {
        if (hasNull) {
            return Movie(
                jsonObject.getString("Title"),
                jsonObject.getString("Year"),
                jsonObject.getString("Rated"),
                jsonObject.getString("Released"),
                jsonObject.getString("Runtime"),
                jsonObject.getString("Genre"),
                jsonObject.getString("Director"),
                jsonObject.getString("Writer"),
                jsonObject.getString("Actors"),
                jsonObject.getString("Plot"),
                jsonObject.getString("Language"),
                jsonObject.getString("Country"),
                jsonObject.getString("Awards"),
                jsonObject.getString("Poster"),
                movieRatingList,
                jsonObject.getString("Metascore"),
                jsonObject.getString("imdbRating"),
                jsonObject.getString("imdbVotes"),
                jsonObject.getString("imdbID"),
                jsonObject.getString("Type"),
                DVD = null,
                boxOffice = null,
                production = null,
                website = null,
                jsonObject.getString("Response")
            )
        } else {
            return Movie(
                jsonObject.getString("Title"),
                jsonObject.getString("Year"),
                jsonObject.getString("Rated"),
                jsonObject.getString("Released"),
                jsonObject.getString("Runtime"),
                jsonObject.getString("Genre"),
                jsonObject.getString("Director"),
                jsonObject.getString("Writer"),
                jsonObject.getString("Actors"),
                jsonObject.getString("Plot"),
                jsonObject.getString("Language"),
                jsonObject.getString("Country"),
                jsonObject.getString("Awards"),
                jsonObject.getString("Poster"),
                movieRatingList,
                jsonObject.getString("Metascore"),
                jsonObject.getString("imdbRating"),
                jsonObject.getString("imdbVotes"),
                jsonObject.getString("imdbID"),
                jsonObject.getString("Type"),
                jsonObject.getString("DVD"),
                jsonObject.getString("BoxOffice"),
                jsonObject.getString("Production"),
                jsonObject.getString("Website"),
                jsonObject.getString("Response")
            )
        }
    }

    inner class loadIMDbDataJSON(var currentMovieList: ArrayList<Movie>, var currentArrayID: Int) : AsyncTask<String, String, String>() {
        override fun doInBackground(vararg params: String?): String {
            var json: String
            val connection = URL(params[0]).openConnection() as HttpURLConnection
            try {
                connection.connect()
                json = connection.inputStream.use {
                    it.reader().use {
                        reader -> reader.readText()
                    }
                }
            } finally {
                connection.disconnect()
            }
            return json
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)

            val jsonObject = JSONObject(result!!)

            var movieRatingList:ArrayList<MovieRating> = arrayListOf()
            println(jsonObject)
            if (jsonObject.getJSONArray("Ratings").length() != 0) {
                jsonObject.getJSONArray("Ratings").let {
                    (0 until jsonObject.getJSONArray("Ratings").length()).forEach {
                        movieRatingList.add(
                            MovieRating(
                                jsonObject.getJSONArray("Ratings").getJSONObject(it)
                                    .getString("Source"),
                                jsonObject.getJSONArray("Ratings").getJSONObject(it)
                                    .getString("Value")
                            )
                        )
                    }
                }
            } else movieRatingList.add(MovieRating("", ""))

            Log.d("ID", DataGlobal.currentIDArray.toString())

//            ZAMENJENO SA KODOM ISPOD
//            if (currentArrayID == 3 || currentArrayID == 4) {
//                currentMovieList.add(
//                    createMovie(jsonObject, movieRatingList, true)
//                )
//            } else {
//                currentMovieList.add(
//                    createMovie(jsonObject, movieRatingList, false)
//                )
//            }

            if (jsonObject.getString("Type") == "movie") {
                currentMovieList.add(
                    createMovie(jsonObject, movieRatingList, false)
                )
            } else {
                currentMovieList.add(
                    createMovie(jsonObject, movieRatingList, true)
                )
            }


        }
    }

    inner class loadImageFromURL(val imageView: ImageView) : AsyncTask<String, Void, Bitmap?>() {
        override fun doInBackground(vararg params: String?): Bitmap? {
            println("loadImageFromURL.doInBackground")
            var image: Bitmap? = null
            val url = params[0]

            try {
                val imageFromAPI = URL(url).openStream()
                image = BitmapFactory.decodeStream(imageFromAPI)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return image
        }

        override fun onPostExecute(result: Bitmap?) {
            super.onPostExecute(result)
            imageView.setImageBitmap(result)
        }
    }
}

data class Movie(
    val title: String,
    val year: String,
    val rated: String,
    val released: String,
    val runtime: String,
    val genre: String,
    val director: String,
    val writer: String,
    val actors: String,
    val plot: String,
    val language: String,
    val country: String,
    val awards: String,
    val poster: String,
    val ratings: ArrayList<MovieRating>?,
    val metascore: String,
    val imdbRating: String,
    val imdbVotes: String,
    val imdbID: String,
    val type: String,
    val DVD: String?,
    val boxOffice: String?,
    val production: String?,
    val website: String?,
    val response: String
)

data class MovieRating(
    val source: String,
    val value: String
)