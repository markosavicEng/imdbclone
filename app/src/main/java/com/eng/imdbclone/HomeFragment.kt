package com.eng.imdbclone

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import java.lang.Exception
import java.net.URL

class HomeFragment : Fragment() {

    var fragmentView: View? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        println("POKRECE SE FRAGMENT")
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_home, container, false)

        fragmentView = view
        view.findViewById<TextView>(R.id.plusbutton).setOnClickListener {
            Log.d("allthebest: ",
                DataGlobal.allTheBestMoviesComingThisSummerMovieList.count().toString()
            )
            Log.d("upcoming: ",
                DataGlobal.upcomingHorrorMoviesWeCantWaitToSeeMovieList.count().toString()
            )
            Log.d("whattvshows: ",
                DataGlobal.whatTVShowsAreRenewedOrCanceledMovieList.count().toString()
            )
            Log.d("allthelatest: ",
                DataGlobal.allTheLatestMovieAndTVPostersMovieList.count().toString()
            )
        }
        setFeaturedTodayImages()
        setFanFavoritesData()

        return view
    }

    fun setFanFavoritesData() {
        println("POKRECE SE ADAPTER U FRAGMENT")
        val adapter = MovieCell1Adapter(DataGlobal.fanFavoritesMovieList)
        val recyclerView = fragmentView?.findViewById<RecyclerView>(R.id.fanFavoritesRecyclerView)
        recyclerView?.adapter = adapter
        recyclerView?.setLayoutManager(LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false))
    }

    inner class loadImageFromURL(val imageView: ImageView) : AsyncTask<String, Void, Bitmap?>() {
        override fun doInBackground(vararg params: String?): Bitmap? {
            var image: Bitmap? = null
            val url = params[0]

            try {
                val imageFromAPI = URL(url).openStream()
                image = BitmapFactory.decodeStream(imageFromAPI)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return image
        }

        override fun onPostExecute(result: Bitmap?) {
            super.onPostExecute(result)
            imageView.setImageBitmap(result)
        }
    }

    fun setFeaturedTodayImages() {
        println(DataGlobal.allTheBestMoviesComingThisSummerMovieList.count())
        if (DataGlobal.allTheBestMoviesComingThisSummerMovieList.count() > 0) {
            loadImageFromURL(fragmentView!!.findViewById(R.id.image1_1)).execute(DataGlobal.allTheBestMoviesComingThisSummerMovieList[5].poster)
            loadImageFromURL(fragmentView!!.findViewById(R.id.image1_2)).execute(DataGlobal.allTheBestMoviesComingThisSummerMovieList[17].poster)
            loadImageFromURL(fragmentView!!.findViewById(R.id.image1_3)).execute(DataGlobal.allTheBestMoviesComingThisSummerMovieList[24].poster)
        }

        loadImageFromURL(fragmentView!!.findViewById(R.id.image2_1)).execute("https://m.media-amazon.com/images/M/MV5BMTM4MTA4MDc2MF5BMl5BanBnXkFtZTcwNTE1MTcxOA@@._V1_FMjpg_UX480_.jpg")
        loadImageFromURL(fragmentView!!.findViewById(R.id.image2_2)).execute("https://m.media-amazon.com/images/M/MV5BMjc0NDkwMzY0M15BMl5BanBnXkFtZTgwNzkyMzgxMzI@._V1_FMjpg_UX600_.jpg")

        if (DataGlobal.upcomingHorrorMoviesWeCantWaitToSeeMovieList.count() > 0) {
                loadImageFromURL(fragmentView!!.findViewById(R.id.image3_1)).execute(DataGlobal.upcomingHorrorMoviesWeCantWaitToSeeMovieList[1].poster)
                loadImageFromURL(fragmentView!!.findViewById(R.id.image3_2)).execute(DataGlobal.upcomingHorrorMoviesWeCantWaitToSeeMovieList[8].poster)
                loadImageFromURL(fragmentView!!.findViewById(R.id.image3_3)).execute(DataGlobal.upcomingHorrorMoviesWeCantWaitToSeeMovieList[16].poster)
        }

        loadImageFromURL(fragmentView!!.findViewById(R.id.image4_1)).execute("https://m.media-amazon.com/images/M/MV5BZTVlYjhhZTAtOWM5Mi00M2E1LWI5ZGUtODU4Yzc2YzQ4N2FkXkEyXkFqcGdeQWplZmZscA@@._V1_SP330,330,0,C,0,0,0_CR65,90,200,150_PIimdb-blackband-204-14,TopLeft,0,0_PIimdb-blackband-204-28,BottomLeft,0,1_CR0,0,200,150_PIimdb-bluebutton-big,BottomRight,-1,-1_ZAClip,4,123,16,196,verdenab,8,255,255,255,1_ZAon%2520IMDb,4,1,14,196,verdenab,7,255,255,255,1_ZA06%253A26,164,1,14,36,verdenab,7,255,255,255,1_PIimdb-HDIconMiniWhite,BottomLeft,4,-2_ZAReflected%2520on%2520Screen%253A%2520C%252E%252E%252E%2520%2528S4%252EE21%2529,24,138,14,176,arialbd,7,255,255,255,1_.jpg")

        if (DataGlobal.whatTVShowsAreRenewedOrCanceledMovieList.count() > 0) {
                loadImageFromURL(fragmentView!!.findViewById(R.id.image5_1)).execute(DataGlobal.whatTVShowsAreRenewedOrCanceledMovieList[3].poster)
                loadImageFromURL(fragmentView!!.findViewById(R.id.image5_2)).execute(DataGlobal.whatTVShowsAreRenewedOrCanceledMovieList[10].poster)
                loadImageFromURL(fragmentView!!.findViewById(R.id.image5_3)).execute(DataGlobal.whatTVShowsAreRenewedOrCanceledMovieList[12].poster)
        }

        loadImageFromURL(fragmentView!!.findViewById(R.id.image6_1)).execute("https://m.media-amazon.com/images/M/MV5BMTYzODU2OTI2Nl5BMl5BanBnXkFtZTgwMTUyMTAwMzE@._V1_.jpg")
        loadImageFromURL(fragmentView!!.findViewById(R.id.image6_2)).execute("https://static01.nyt.com/images/2020/02/09/fashion/carpet-2020-3208/carpet-2020-3208-mobileMasterAt3x-v3.jpg")

        loadImageFromURL(fragmentView!!.findViewById(R.id.image7_1)).execute("https://m.media-amazon.com/images/M/MV5BNDhmMjg4NzEtYTM3Yy00ZjQyLWFkNWEtODhmMWRhODdkYmNmXkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_.jpg")

        if (DataGlobal.allTheLatestMovieAndTVPostersMovieList.count() > 0) {
                loadImageFromURL(fragmentView!!.findViewById(R.id.image8_1)).execute(DataGlobal.allTheLatestMovieAndTVPostersMovieList[2].poster)
                loadImageFromURL(fragmentView!!.findViewById(R.id.image8_2)).execute(DataGlobal.allTheLatestMovieAndTVPostersMovieList[4].poster)
                loadImageFromURL(fragmentView!!.findViewById(R.id.image8_3)).execute(DataGlobal.allTheLatestMovieAndTVPostersMovieList[5].poster)
        }

        loadImageFromURL(fragmentView!!.findViewById(R.id.image9_1)).execute("https://m.media-amazon.com/images/M/MV5BMjMwMDcyNjc0M15BMl5BanBnXkFtZTgwMzI0MDY1MTE@._V1_.jpg")
        loadImageFromURL(fragmentView!!.findViewById(R.id.image9_2)).execute("https://imagez.tmz.com/image/83/o/2021/06/13/83ca2c523360429da8e3c351faf316e5_lg.jpg")
    }
}