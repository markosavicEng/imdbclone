package com.eng.imdbclone

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import java.lang.Exception
import java.net.URL

class MovieCell1Adapter(val fanFavoritesMovieList: ArrayList<Movie>) : RecyclerView.Adapter<CustomViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cell = layoutInflater.inflate(R.layout.movie_cell_1, parent, false)
        return CustomViewHolder(cell)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        loadImageFromURL(holder.view.findViewById(R.id.movieCell1FrontCoverImageView)).execute(fanFavoritesMovieList[position].poster)
        holder.view.findViewById<TextView>(R.id.movieCell1RatingTextView).text = fanFavoritesMovieList[position].imdbRating
        holder.view.findViewById<TextView>(R.id.movieCell1TitleTextView).text = fanFavoritesMovieList[position].title

        var runtime: String
        if (fanFavoritesMovieList[position].runtime != "N/A") {
            runtime = fanFavoritesMovieList[position].runtime.filter { it.isDigit() }
            runtime = "${runtime.toInt() / 60}h ${runtime.toInt() % 60}m"
        } else {
            runtime = "N/A"
        }

        if (fanFavoritesMovieList[position].type == "movie") {
            holder.view.findViewById<TextView>(R.id.movieCell1DetailsTextView).text = fanFavoritesMovieList[position].year + " " + fanFavoritesMovieList[position].rated + " " + runtime
        } else {
            holder.view.findViewById<TextView>(R.id.movieCell1DetailsTextView).text = fanFavoritesMovieList[position].year + " "
        }
    }

    override fun getItemCount(): Int {
        return fanFavoritesMovieList.count()
    }

    inner class loadImageFromURL(val imageView: ImageView) : AsyncTask<String, Void, Bitmap?>() {
        override fun doInBackground(vararg params: String?): Bitmap? {
            println("loadImageFromURL.doInBackground")
            var image: Bitmap? = null
            val url = params[0]

            try {
                val imageFromAPI = URL(url).openStream()
                image = BitmapFactory.decodeStream(imageFromAPI)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return image
        }

        override fun onPostExecute(result: Bitmap?) {
            super.onPostExecute(result)
            imageView.setImageBitmap(result)
        }
    }
}

class CustomViewHolder(val view: View) : RecyclerView.ViewHolder(view)